from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts

def path_curriculum_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'curriculum/'

    return os.path.join(path, filename)
# Create your models here.
class Contact(models.Model):

    contact_type = (
        (1, _('interés en peliculas de línea')),
        (2, _('interés en productos de línea')),
        (3, _('interés en producto de especialidad')),
        (4, _('contacto como proveedor')),
        (5, _('contacto como cliente')),
        (6, _('contacto como interés en trabajar')),
        (7, _('cotizar peliculas de línea')),
        (8, _('cotizar productos de especialidad')),
        (9, _('cotizar productos de línea')),
        (10, _('otros')),
    )

    type_contact = models.IntegerField(choices=contact_type, default=1)
    application_interest = models.CharField(max_length=255, blank=True, null=True)
    product = models.CharField(max_length=255, blank=True, null=True)
    quantity = models.CharField(max_length=255, blank=True, null=True)
    wide = models.CharField(max_length=255, blank=True, null=True)
    calibre = models.CharField(max_length=255, blank=True, null=True)
    company_name = models.CharField(max_length=255, blank=True, null=True)
    contact_name = models.CharField(max_length=255, blank=True, null=True)
    product_interest = models.CharField(max_length=255, blank=True, null=True)
    phone_number = models.CharField(max_length=255, blank=True, null=True)
    mail = models.CharField(max_length=255, blank=True, null=True)
    stall = models.CharField(max_length=255, blank=True, null=True)
    curruculum = models.FileField(upload_to='curriculum/', blank=True, null=True)
    organic_product = models.CharField(max_length=255, blank=True, null=True)
    line_products = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'contact'
        verbose_name_plural = 'contact'