from django.contrib import admin
from .models import Contact
# Register your models here.
class ContactAdmin(admin.ModelAdmin):
    list_display = ('type_contact','application_interest','product','quantity','wide','calibre','company_name','contact_name','product_interest','phone_number','mail','stall','curruculum','organic_product','line_products')
    search_fields = ('type','mail')
