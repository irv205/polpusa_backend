from rest_framework import serializers, exceptions
from . import models

class ContactSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Contact
        fields = ('type_contact','application_interest','product','quantity','wide','calibre','company_name','contact_name','product_interest','phone_number','mail','stall','curruculum','organic_product','line_products')