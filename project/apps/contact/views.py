from django.shortcuts import render
import json
from uuid import uuid4
from rest_framework import status
from rest_framework.views import Response, APIView
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import exceptions, permissions
from project.common.pagination import PageNumberPagination
from django.http import HttpResponse
from . import serializers
from . import models
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import viewsets, status, exceptions, permissions
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage
from django.core.mail import send_mail

# Create your views here.
class ContactView(viewsets.ModelViewSet):

    model = models.Contact
    permission_classes = ( )
    serializer_class = serializers.ContactSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            is_active=True,       
        )
        proveedor = 'mduarte@polpusa.com'
        cliente = 'aplopez@polpusa.com'
        cliente2 = 'ventas@polpusa.com'
        trabajo = 'rsanchez@polpusa.com'
        emailPolpusa = 'info@polpusa.com'
        
        type_contact = str(item.type_contact)
        application = item.application_interest
        product = item.product
        quantity = item.quantity
        wide = item.wide
        calibre = item.calibre
        company = item.company_name
        contact = item.contact_name
        product_interest = item.product_interest
        phone = item.phone_number
        mail = item.mail
        stall = item.stall
        organic = item.organic_product
        line = item.line_products
        cv = str(item.curruculum)

        if type_contact == '1':
            print("interés en peliculas de línea")
            email = EmailMessage(
            'interés en peliculas de línea',
            'Aplicacion de interes: '+application+'\n'
            'Cantidad: '+quantity+'\n'
            'Ancho: '+wide+'\n'
            'Calibre: '+calibre+'\n'
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '2':
            print("interés en productos de línea")
            email = EmailMessage(
            'interés en productos de línea',
            'Producto: '+product+'\n'
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '3':
            print("interés en producto de especialidad")
            email = EmailMessage(
            'interés en producto de especialidad',
            'Aplicacion de interes: '+application+'\n'
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '4':
            print("contacto como proveedor")
            email = EmailMessage(
            'contacto como proveedor',
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '5':
            print("contacto como cliente")
            email = EmailMessage(
            'contacto como cliente',
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Producto de interes: '+product_interest+'\n'
            'Correo: '+mail+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '6':
            print("contacto como interés en trabajar")
            email = EmailMessage(
            'contacto como interés en trabajar',
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Puesto'+stall+'\n'
            'CV: '+settings.URL_SERVER+'/media/'+cv+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '7':
            print("cotizar peliculas de línea")
            email = EmailMessage(
            'cotizar peliculas de línea',
            'Aplicacion de interes: '+application+'\n'
            'Cantidad: '+quantity+'\n'
            'Ancho: '+wide+'\n'
            'Calibre: '+calibre+'\n'
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '8':
            print("cotizar productos de especialidad")
            email = EmailMessage(
            'cotizar productos de especialidad',
            'Aplicacion de interes: '+application+'\n'
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '9':
            print("cotizar productos de línea")
            email = EmailMessage(
            'cotizar productos de línea',
            'Compañia: '+company+'\n'
            'Contacto: '+contact+'\n'
            'Telefono: '+phone+'\n'
            'Correo: '+mail+'\n'
            'Organico: '+organic+'\n'
            'Linea de productos: '+line+'\n',
            emailPolpusa, [cliente,cliente2])
            email.send()
        if type_contact == '10':
            return Response({'message': 'Esta opcion no existe'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        return item