from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts

# Create your models here.
class Description(FieldDefaultsAbstracts):

    description_type = (
        (1, _("sustentabilidad en plastico")),
        (2, _("Nuestras acciones")),
        (3, _("polpusaverde breve")),
        (4, _("polpusaverde larga")),
        (5, _("productos especiales")),
        (6, _("polpusaverde viñetas")),
    )

    section_type = (
        (1, _("sustentability")),
    )

    description = models.CharField(max_length=1500, blank=True, null=True)
    description_type = models.IntegerField(choices=description_type, default=1)
    section = models.IntegerField(choices=section_type, default=1)

    def __str__(self):
        return self.id
    
    class Meta:
        verbose_name = "Description"
        verbose_name_plural = "Descriptions" 


class TranslationDescriptions(FieldDefaultsAbstracts):

    languaje_TYPE = (
        (1, _('English')),
    )

    translation = models.ForeignKey(Description, related_name="translation", blank = True, null = True, on_delete=models.CASCADE)
    description = models.CharField(max_length=1500, blank=True, null=True)
    language = models.IntegerField(choices=languaje_TYPE, default=1)

    def __str__(self):
        return self.id

    class Meta:
        verbose_name = 'translation'
        verbose_name_plural = 'translation description'