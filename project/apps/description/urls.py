from django.conf.urls import url
from rest_framework import routers
from project.apps.security import views as secview
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView
)

router = routers.DefaultRouter()

router.register(r'description', views.DescriptionView, base_name='objects')
router.register(r'translation-description', views.TranslationDescriptionView, base_name='objects')
router.register(r'description-translations', views.GetDescriptionAll, base_name='objects')

urlpatterns = []

urlpatterns += router.urls