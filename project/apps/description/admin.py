from django.contrib import admin
from .models import Description
from .models import TranslationDescriptions
# Register your models here.
class DescriptionAdmin(admin.ModelAdmin):
    list_display = ('id','description','description_type','section')
    search_fields = ('id', 'section')

class TranslationDescriptionAdmin(admin.ModelAdmin):
    list_display = ('id','translation','description','language')
    search_fields = ('id', 'language')

admin.site.register(Description, DescriptionAdmin)
admin.site.register(TranslationDescriptions, TranslationDescriptionAdmin)
