from rest_framework import serializers, exceptions
from . import models

class DescriptionSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Description
        fields = ('id','description','description_type','section')

class TranslationDescriptionSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.TranslationDescriptions
        fields = ('id','translation','description','language')

class GetAllDescriptionSerializers(serializers.ModelSerializer):

    translation = TranslationDescriptionSerializers(many = True)

    class Meta:
        model = models.Description
        fields = ('id','description','description_type','section','translation')