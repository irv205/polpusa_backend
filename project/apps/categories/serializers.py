from rest_framework import serializers, exceptions
from . import models

class CategoriesSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Categories
        fields = ('id', 'title', 'img', 'description','order')

class TraslationCategoriesSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.TranslationCategories
        fields = ('id','title','description','language','category_id')

class GetAllCategoriesSerializers(serializers.ModelSerializer):
    category = TraslationCategoriesSerializers(many = True)

    class Meta:
        model = models.Categories
        fields = ('id', 'title', 'img', 'description','category','order')
