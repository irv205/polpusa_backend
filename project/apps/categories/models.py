from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts

# Create your models here.
def path_image_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'categories/'

    return os.path.join(path, filename)

class Categories(FieldDefaultsAbstracts):

    title = models.CharField(max_length=50, blank=True, null=True)
    img = models.FileField(upload_to='categories/', blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    order = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['order']
        verbose_name_plural = 'Categories'

class TranslationCategories(FieldDefaultsAbstracts):

    languaje_TYPE = (
        (1, _('English')),
    )

    category_id = models.ForeignKey(Categories, related_name="category", blank=True, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    language = models.IntegerField(choices=languaje_TYPE, default=1)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'traslation'
        verbose_name_plural = 'traslations categories'