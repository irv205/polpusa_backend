# Generated by Django 2.2.2 on 2020-10-22 17:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '0002_auto_20200903_1116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='categories',
            name='img',
            field=models.FileField(blank=True, null=True, upload_to='categories/'),
        ),
    ]
