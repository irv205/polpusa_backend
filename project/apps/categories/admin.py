from django.contrib import admin
from .models import Categories
from .models import TranslationCategories

# Register your models here.
class CategoriesAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'img', 'description','order')
    search_fields = ('id','title')

class TraslationCategoriesAdmin(admin.ModelAdmin):
    list_display = ('id','title','description','language')
    search_fields = ('id','title')

admin.site.register(Categories, CategoriesAdmin)
admin.site.register(TranslationCategories, TraslationCategoriesAdmin)