from django.conf.urls import url
from rest_framework import routers
from project.apps.security import views as secview
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView
)

router = routers.DefaultRouter()

router.register(r'product', views.ProductViews, base_name='objects')
router.register(r'product-EN', views.ProductENViews, base_name='objects')
router.register(r'specs', views.SpecsViews, base_name='objects')
router.register(r'properties', views.PropertiesViews, base_name='objects')
router.register(r'properties-EN', views.PropertiesENViews, base_name='objects')
router.register(r'advantage', views.AdvantageViews, base_name='objects')
router.register(r'advantage-EN', views.AdvantageENViews, base_name='objects')
router.register(r'product-all', views.ProductAllViews, base_name='objects')

urlpatterns = []

urlpatterns += router.urls