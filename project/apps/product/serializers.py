from rest_framework import serializers, exceptions
from . import models
from project.apps.uploadFile.serializers import UploadFileSerializers

class ProductSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Product
        fields = ('id','category_id','name','product_photo','footer','description','photo_specifications','data_sheet','images_type','image_use','type_bag','order')

class ProductENSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.ProductEN
        fields = ('id','product_id','name','footer','description','photo_specifications','data_sheet',)

class SpecsSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Specs
        fields = ('id','product_id','field_1','field_2','field_3','field_4','field_5',)

class PropertiesSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Properties
        fields = ('id','product_id','property_product',)

class PropertiesENSerializers(serializers.ModelSerializer):

    class Meta: 
        model = models.PropertiesEN
        fields = ('id','propertiesEN','property_product',)

class AdvantageSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Advantage
        fields = ('id','product_id','advantage_product',)

class AdvantageENSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.AdvantageEN
        fields = ('id','advantageEN','advantage_product',)

class GetAllProducts(serializers.ModelSerializer):


    images_type = UploadFileSerializers(many = True)
    image_use = UploadFileSerializers(many = True)
    product_EN = ProductENSerializers(many = True)
    specs_id = SpecsSerializers(many = True)
    properties_id = PropertiesSerializers(many = True)
    properties_EN = PropertiesENSerializers(many = True)
    advantage_id = AdvantageSerializers(many = True)
    advantage_EN = AdvantageENSerializers(many = True)

    class Meta:
        model = models.Product
        fields = ('id','category_id','name','product_photo','footer','description','photo_specifications','data_sheet','order','images_type','image_use','type_bag','product_EN','specs_id','properties_id','properties_EN','advantage_id','advantage_EN')