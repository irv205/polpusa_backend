# Generated by Django 2.2.2 on 2020-09-17 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0012_auto_20200903_1116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='type_bag',
            field=models.IntegerField(choices=[(1, 'biodegradable'), (2, 'reciclado'), (3, 'reutilizable'), (4, 'compostable'), (5, 'reciclable'), (6, 'otro'), (7, 'sin asignacion')], default=7),
        ),
    ]
