# Generated by Django 2.2.2 on 2020-10-22 18:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0014_auto_20201022_1255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(blank=True, max_length=2000, null=True),
        ),
        migrations.AlterField(
            model_name='producten',
            name='description',
            field=models.CharField(blank=True, max_length=2000, null=True),
        ),
    ]
