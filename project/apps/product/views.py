from django.shortcuts import render
import json
from uuid import uuid4
from rest_framework import status
from rest_framework.views import Response, APIView
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import exceptions, permissions
from project.common.pagination import PageNumberPagination
from django.http import HttpResponse
from . import serializers
from . import models
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import viewsets, status, exceptions, permissions
from django.conf import settings
from django.shortcuts import get_object_or_404

# Create your views here.
class ProductViews(viewsets.ModelViewSet):

    model = models.Product
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.ProductSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        category_id = kwargs.get('category_id', None)
        name = kwargs.get('name', None)
        type_bag = kwargs.get('type_bag', None)

        queryset = queryset.filter(
            is_active = True
        )

        if category_id:
            queryset = queryset.filter(
                category_id = category_id
            )
        if name:
            queryset = queryset.filter(
                name = name
            )
        if type_bag:
            queryset = queryset.filter(
                type_bag = type_bag
            )
        return queryset

    def perform_create(self, serializer):
        products_order = self.model.objects.order_by('order').last()
        if products_order:
            item = serializer.save(
                owner = self.request.user,
                order = products_order.order+100,
                is_active=True,       
            )
        else:
            item = serializer.save(
                owner = self.request.user,
                order = 100,
                is_active=True,       
            )
        return item
    
    def perform_update(self, serializer):
        #print(serializer)
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class ProductENViews(viewsets.ModelViewSet):

    model = models.ProductEN
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.ProductENSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        #print(serializer)
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class SpecsViews(viewsets.ModelViewSet):

    model = models.Specs
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.SpecsSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class PropertiesViews(viewsets.ModelViewSet):

    model = models.Properties
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.PropertiesSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class PropertiesENViews(viewsets.ModelViewSet):

    model = models.PropertiesEN
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.PropertiesENSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class AdvantageViews(viewsets.ModelViewSet):

    model = models.Advantage
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.AdvantageSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class AdvantageENViews(viewsets.ModelViewSet):

    model = models.AdvantageEN
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.AdvantageENSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class ProductAllViews(viewsets.ModelViewSet):

    model = models.Product
    permission_classes = ()
    serializer_class = serializers.GetAllProducts

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        category_id = kwargs.get('category_id', None)
        name = kwargs.get('name', None)
        type_bag = kwargs.get('type_bag', None)

        queryset = queryset.filter(
            is_active = True
        )

        if category_id:
            queryset = queryset.filter(
                category_id = category_id
            )
        if name:
            queryset = queryset.filter(
                name = name
            )
        if type_bag:
            queryset = queryset.filter(
                type_bag = type_bag
            )
        return queryset
