from django.contrib import admin
from .models import Product
from .models import Specs
# from .models import Properties
# from .models import Advantage

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    list_display = ('name','product_photo','footer','description','photo_specifications','data_sheet','order')
    search_fields = ('name','id')

class ProductENAdmin(admin.ModelAdmin):
    list_display = ('name','footer','description','photo_specifications','data_sheet')

class SpecsAdmin(admin.ModelAdmin):
    list_display = ('field_1','field_2','field_3','field_4','field_5')

class PropertiesAdmin(admin.ModelAdmin):
    list_display = ('property_product')
    search_fields = ('id','property_product')

class PropertiesENAdmin(admin.ModelAdmin):
    list_display = ('property_product')

class Advantage(admin.ModelAdmin):
    list_display = ('advantage_product')
    search_fields = ('id','advantage_product')

class AdvantageENAdmin(admin.ModelAdmin):
    list_display = ('advantage_product')