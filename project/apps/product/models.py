from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts
from project.apps.categories.models import Categories
from project.apps.uploadFile.models import UploadFile

def path_product_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'product/'

    return os.path.join(path, filename)

def path_data_sheet_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'dataSheet/'

    return os.path.join(path, filename)

def path_specifications_sheet_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'specs/'

    return os.path.join(path, filename)

# Create your models here.
class Product(FieldDefaultsAbstracts):

    product_TYPE = (
        (1, _("biodegradable")),
        (2, _("reciclado")),
        (3, _("reutilizable")),
        (4, _("compostable")),
        (5, _("reciclable")),
        (6, _("otro")),
        (7, _("sin asignacion")),
    )

    category_id = models.ForeignKey(Categories, related_name="category_id", blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)
    product_photo = models.FileField(upload_to='product/', blank=True, null=True)
    footer = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    photo_specifications = models.FileField(upload_to='specs/', blank=True, null=True)
    data_sheet = models.FileField(upload_to='dataSheet/', blank=True, null=True)
    images_type = models.ManyToManyField(UploadFile, related_name="imgType", blank=True)
    image_use = models.ManyToManyField(UploadFile, related_name="imgUse", blank=True)
    type_bag = models.IntegerField(choices=product_TYPE, default=7)
    order = models.FloatField(max_length=9999, default=100)

    def delete(self, *args, **kwargs):
        self.product_photo.delete()
        self.photo_specifications.delete()
        self.data_sheet.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-order']

class ProductEN(FieldDefaultsAbstracts):

    product_id = models.ForeignKey(Product, related_name="product_EN", blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)
    footer = models.CharField(max_length=50, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)
    photo_specifications = models.FileField(upload_to='specs/', blank=True, null=True)
    data_sheet = models.FileField(upload_to='dataSheet/', blank=True, null=True)

    def delete(self, *args, **kwargs):
        self.photo_specifications.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.product_id

    class Meta:
        ordering = ['-id']

class Specs(FieldDefaultsAbstracts):
    product_id = models.ForeignKey(Product, related_name="specs_id", blank=True, null=True, on_delete=models.CASCADE)
    field_1 = models.CharField(max_length=50, blank=True, null=True)
    field_2 = models.CharField(max_length=50, blank=True, null=True)
    field_3 = models.CharField(max_length=50, blank=True, null=True)
    field_4 = models.CharField(max_length=50, blank=True, null=True)
    field_5 = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.id
    
    class Meta:
        ordering = ['id']

class Properties(FieldDefaultsAbstracts):

    product_id = models.ForeignKey(Product, related_name="properties_id", blank=True, null=True, on_delete=models.CASCADE)
    property_product = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.id
    
    class Meta:
        ordering = ['id']

class PropertiesEN(FieldDefaultsAbstracts):

    propertiesEN = models.ForeignKey(Product, related_name="properties_EN", blank=True, null=True, on_delete=models.CASCADE)
    property_product = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.id
    
    class Meta:
        ordering = ['id']

class Advantage(FieldDefaultsAbstracts):

    product_id = models.ForeignKey(Product, related_name="advantage_id", blank=True, null=True, on_delete=models.CASCADE)
    advantage_product = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.id
    
    class Meta:
        ordering = ['id']

class AdvantageEN(FieldDefaultsAbstracts):

    advantageEN = models.ForeignKey(Product, related_name="advantage_EN", blank=True, null=True, on_delete=models.CASCADE)
    advantage_product = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.id

    class Meta:
        ordering = ['id']