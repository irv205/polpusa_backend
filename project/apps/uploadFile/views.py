from django.shortcuts import render
import json
from uuid import uuid4
from rest_framework import status
from rest_framework.views import Response, APIView
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import exceptions, permissions
from project.common.pagination import PageNumberPagination
from django.http import HttpResponse
from . import serializers
from . import models
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import viewsets, status, exceptions, permissions
from django.conf import settings
from django.shortcuts import get_object_or_404
from .models import UploadFile
import ast

# Create your views here.
class UploadFileView(viewsets.ModelViewSet):

    model = models.UploadFile
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.UploadFileSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        name = kwargs.get('name', None)
        type = kwargs.get('type', None)

        queryset = queryset.filter(
            is_active = True
        )

        if name:
            queryset = queryset.filter(
                name = name
            )
        if type:
            queryset = queryset.filter(
                type = type
            )
        return queryset

    
    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class TranslationUploadFIle(viewsets.ModelViewSet):

    model = models.UploadFIleEN
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.UploadFIleENSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 

        queryset = queryset.filter(
            is_active = True
        )

        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        #print(serializer)
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class GetAll(viewsets.ModelViewSet):

    model = models.UploadFile
    permission_classes = ( )
    serializer_class = serializers.GetAllUploadFIlesSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        name = kwargs.get('name', None)
        type = kwargs.get('type', None)

        queryset = queryset.filter(
            is_active = True
        )
        if name:
            queryset = queryset.filter(
                name = name
            )
        if type:
            queryset = queryset.filter(
                type = type
            )
        return queryset