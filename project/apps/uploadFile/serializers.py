from rest_framework import serializers, exceptions
from . import models

class UploadFileSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.UploadFile
        fields = ('id','name','u_file','type')

class UploadFIleENSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.UploadFIleEN
        fields = ('id','UploadFile_id', 'name', 'u_file','language')

class GetAllUploadFIlesSerializers(serializers.ModelSerializer):
    uploadID = UploadFIleENSerializers(many = True)

    class Meta:
        model = models.UploadFile
        fields = ('id','name','u_file','type', 'uploadID')