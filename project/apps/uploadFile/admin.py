from django.contrib import admin
from .models import UploadFile
from .models import UploadFIleEN
# Register your models here.
class UploadFileAdmin(admin.ModelAdmin):
    list_display = ('id','name','u_file','type')
    search_fields = ('id','type')

class UploadFileENAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'u_file','language')
    search_fields = ('id','name')

admin.site.register(UploadFile, UploadFileAdmin)
admin.site.register(UploadFIleEN, UploadFileENAdmin)
