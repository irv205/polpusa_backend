from django.conf.urls import url
from rest_framework import routers
from project.apps.security import views as secview
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView
)

router = routers.DefaultRouter()

router.register(r'upload-file', views.UploadFileView, base_name='objects')
router.register(r'upload-file-en', views.TranslationUploadFIle, base_name='objects')
router.register(r'upload-all', views.GetAll, base_name='objects')

urlpatterns = []

urlpatterns += router.urls