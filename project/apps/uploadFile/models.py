from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts

def path_img_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'upload/'

    return os.path.join(path, filename)

# Create your models here.
class UploadFile(FieldDefaultsAbstracts):

    file_type = (
        (1, _("img")),
        (2, _("pdf")),
        (3, _("other")),
        (4, _("Catalogue")),
    )

    name = models.CharField(max_length=50, blank=True, null=True)
    u_file = models.FileField(upload_to='upload/', blank=True, null=True)
    type = models.IntegerField(choices=file_type, default=3)

    def delete(self, *args, **kwargs):
        self.u_file.delete()
        super().delete(*args, **kwargs)

    class Meta:
        ordering = ['-id']
        verbose_name = 'Upload file'
        verbose_name_plural = "Upload files"

    def __str__(self):
        return self.type

class UploadFIleEN(FieldDefaultsAbstracts):

    languaje_TYPE = (
        (1, _("English")),
    )

    UploadFile_id = models.ForeignKey(UploadFile, related_name="uploadID", blank=True, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, blank=True, null=True)
    u_file = models.FileField(upload_to='upload/', blank=True, null=True)
    language = models.IntegerField(choices=languaje_TYPE, default=1)

    def delete(self, *args, **kwargs):
        self.u_file.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'UploadFileEn'
        verbose_name_plural = 'UploadFileEn'