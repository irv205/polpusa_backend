from django.conf.urls import url
from rest_framework import routers
from project.apps.security import views as secview
from . import views
from rest_framework_simplejwt.views import (
    TokenObtainPairView, TokenRefreshView
)

router = routers.DefaultRouter()

router.register(r'slides', views.SlidesView, base_name='objects')
router.register(r'traslation-slide', views.TraslationSlidesView, base_name='objects')
router.register(r'slides-traslations', views.GetSlidesAll, base_name='objects')

urlpatterns = []

urlpatterns += router.urls