from rest_framework import serializers, exceptions
from . import models

class SlidesSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Slides
        fields = ('id','title','file_Upload','order','type', 'section')

class TraslationsSlidesSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.TraslationSlides
        fields = ('id','translation','title','file_Upload','language')

class GetAllSlidesSerializers(serializers.ModelSerializer):
    slides = TraslationsSlidesSerializers(many = True)

    class Meta:
        model = models.Slides
        fields = ('id','title','file_Upload','order','type', 'section','slides')