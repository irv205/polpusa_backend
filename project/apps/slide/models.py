from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts

def path_slide_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'slide/'

    return os.path.join(path, filename)

# Create your models here.
class Slides(FieldDefaultsAbstracts):

    slide_type = (
        (1, _("videoBanner")),
        (2, _("ImageBanner")),
        (3, _("carrucel")),
        (4, _("countries")),
        (5, _("certifications")),
        (6, _("img")),
    )

    section_type = (
        (1, _("home")),
        (2, _("product")),
        (3, _("US")),
        (4, _("contact")),
        (5, _("sustentability")),
        (6, _("blog")),
    )

    title = models.CharField(max_length=50, blank=True, null=True)
    file_Upload = models.FileField(upload_to='slide/', blank=True, null=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    type = models.IntegerField(choices=slide_type, default=1)
    section = models.IntegerField(choices=section_type, default=1)

    def delete(self, *args, **kwargs):
        self.file_Upload.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.title
    
    class Meta:
        ordering = ['order']
        verbose_name = 'Slide'
        verbose_name_plural = 'Slides'

class TraslationSlides(FieldDefaultsAbstracts):

    languaje_TYPE = (
        (1, _('English')),
    )

    translation = models.ForeignKey(Slides, related_name="slides", blank=True, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, blank=True, null=True)
    file_Upload = models.FileField(upload_to='slide/', blank=True, null=True)
    language = models.IntegerField(choices=languaje_TYPE, default=1)

    def delete(self, *args, **kwargs):
        self.file_Upload.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta: 
        verbose_name = 'traslation'
        verbose_name_plural = ' traslations slides' 