from django.contrib import admin
from .models import Slides
from .models import TraslationSlides
# Register your models here.
class SlidesAdmin(admin.ModelAdmin):
    list_display = ('id','title','file_Upload','order', 'type', 'section')
    search_fields = ('id','title')

class TraslationSlidesAdmin(admin.ModelAdmin):
    list_display = ('id','title','file_Upload','language')
    search_fields = ('id','title')

admin.site.register(Slides, SlidesAdmin)
admin.site.register(TraslationSlides, TraslationSlidesAdmin)