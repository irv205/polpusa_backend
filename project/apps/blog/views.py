from django.shortcuts import render
import json
from uuid import uuid4
from rest_framework import status
from rest_framework.views import Response, APIView
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import exceptions, permissions
from project.common.pagination import PageNumberPagination
from django.http import HttpResponse
from . import serializers
from . import models
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework import viewsets, status, exceptions, permissions
from django.conf import settings
from django.shortcuts import get_object_or_404

# Create your views here.
class BlogView(viewsets.ModelViewSet):

    model = models.Blog
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.BlogSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        title = kwargs.get('title', None)

        queryset = queryset.filter(
            is_active = True
        )

        if title:
            queryset = queryset.filter(
                title = title
            )
        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class BlogTranslationsView(viewsets.ModelViewSet):

    model = models.TranslationBlog
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = serializers.BlogTranslationSerializers

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        title = kwargs.get('title', None)

        queryset = queryset.filter(
            is_active = True
        )

        if title:
            queryset = queryset.filter(
                title = title
            )
        return queryset

    def perform_create(self, serializer):
        item = serializer.save(
            owner = self.request.user,
            is_active=True,       
        )
        return item
    
    def perform_update(self, serializer):
        serializer.save(
            owner = self.request.user,
            is_active=True, 
        )

    def perform_destroy(self, instance):
        instance.delete()

class GetBlogAll(viewsets.ModelViewSet):

    model = models.Blog
    permission_classes = ()
    serializer_class = serializers.GetAllBlog

    def get_queryset(self):
        queryset = self.model.objects.all()
        return self.filter(queryset)

    def filter(self, queryset):
        kwargs = self.request.GET 
        title = kwargs.get('title', None)
        random = kwargs.get('random', None)

        queryset = queryset.filter(
            is_active = True
        )

        if title:
            queryset = queryset.filter(
                title = title
            )
        if random:
            queryset = self.model.objects.all().order_by('?')
        return queryset
