from django.contrib import admin
from .models import Blog
from .models import TranslationBlog
# Register your models here.
class BlogAdmin(admin.ModelAdmin):
    list_display = ('id','title','date','image','body')
    search_fields = ('id','title')

class TranslationBlogAdmin(admin.ModelAdmin):
    list_display = ('id','title','body','language')
    search_fields = ('id','title')

admin.site.register(Blog, BlogAdmin)
admin.site.register(TranslationBlog, TranslationBlogAdmin)
