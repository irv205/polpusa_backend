from django.db import models
import os
import datetime
from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.translation import ugettext as _
from django.core import signing
from ...common.models import FieldDefaultsAbstracts
# Create your models here.

def path_image_and_rename(obj, filename):
    ext = filename.split('.')[-1]

    filename = '{}.{}'.format(uuid4().hex, ext)

    path = 'blog/'

    return os.path.join(path, filename)

class Blog(FieldDefaultsAbstracts):

    title = models.CharField(max_length=255, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    image = models.FileField(upload_to='blog/', blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    random = models.BooleanField(default=False)

    def delete(self, *args, **kwargs):
        self.image.delete()
        super().delete(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-id']
        verbose_name = 'Blog'
        verbose_name_plural = 'Blogs'

class TranslationBlog(FieldDefaultsAbstracts):

    languaje_TYPE = (
        (1, _('English')),
    )

    blog_id = models.ForeignKey(Blog, related_name='blog', blank=True, null=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=255, blank=True, null=True)
    body = models.TextField(blank=True, null=True)
    language = models.IntegerField(choices=languaje_TYPE, default=1)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = ' translation blog'
        verbose_name_plural = 'translation blogs'