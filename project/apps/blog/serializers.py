from rest_framework import serializers, exceptions
from . import models

class BlogSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.Blog
        fields = ('id','title','date','image','body')

class BlogTranslationSerializers(serializers.ModelSerializer):

    class Meta:
        model = models.TranslationBlog
        fields = ('id','blog_id','title','body','language')

class GetAllBlog(serializers.ModelSerializer):
    blog = BlogTranslationSerializers(many = True)

    class Meta:
        model = models.Blog
        fields = ('id','title','date','image','body','blog')